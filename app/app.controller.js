const path = require("path");
const dirTree = require("directory-tree");
const ejs = require("ejs");
const favicon = require("serve-favicon");

module.exports = {
    
    render: (express, app, directory) => {
        app
        .set("view engine", "ejs")
        .use(favicon(path.join(__dirname, "..", "favicon.ico")))
        .use("/file", express.static(directory))
        .get("/tree/*", (req, res) => {

            let relativeUrl = req.url.replace("/tree", "");
            let parent = req.url !== "/tree/" ? req.url + ".." : null;

            let currentPath = path.join(directory + path.normalize(relativeUrl));

            if(dirTree(currentPath) === null) {
                console.log(relativeUrl);
                console.log(currentPath);
            }

            let files = dirTree(currentPath).children.filter(e => e.type === 'directory')
            .concat(dirTree(currentPath).children.filter(e => e.type === 'file'));

            let params = {
                path: relativeUrl,
                directory: directory + relativeUrl,
                parent: parent,
                files: files
            };
            res.render(path.join(__dirname, "tree"), params);
        })
        .get("/", (req, res) => {
            res.redirect("/tree/");
        });
    }

}