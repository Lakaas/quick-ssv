const express = require("express");
const getPort = require("get-port");
const opn = require("opn");
const appController = require("./app/app.controller");

const app = express();

let dir, port;

async function getDirectory() {
    dir = process.argv[2];
    if(!dir) {
        throw new Error("Missing directory argument"); 
    }
    return 0;
}

async function startServer() {
    //appController.serveFiles(express, app, dir);
    appController.render(express, app, dir);
    app.listen(port);
    return 0;
}

function main() {
    getDirectory()
    .then(res => {
        return getPort();
    })
    .then(res => {
        port = res;
        return startServer();
    })
    .then(res => {
        opn(`http://localhost:${port}/`);
    })
    .catch(err => {
        console.log(err);
    });
}

main();



